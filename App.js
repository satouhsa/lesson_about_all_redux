

// import React, { Component } from 'react'
// import { Alert, Text, View } from 'react-native'
// import { TouchableOpacity } from 'react-native-gesture-handler';

// export class App extends Component {
//   constructor(){
//     super();
//     this.state={
//       change:'',
//     }
//   }

//   heading(){
//     return(
//       <View style={styles.mainTitle}>
//         <Text>Multi Language in React Native</Text>
//       </View>
//     )
//   }

//   button(){
//     return(
//       <TouchableOpacity 
//         onPress={()=>{
//           Alert.alert(
//             'message of alert',
//             [
//               {
//                 text: 'khmer',
//                 onPress: ()=>{
//                   I18n.locale='kh';
//                   this.setState({change:'khmer'});
//                 },
                
//               },
//               {
//                 text:'Korean',
//                 onPress: ()=>{
//                   I18n.locale='kr';
//                   this.setState({change:'Korean'});
//                 },
//               },
//               {
//                 text:'English',
//                 onPress:()=>{
//                   I18n.locale='eg',
//                   this.setState({change:'English'});
//                 }

//               }

//             ]
            
//           )
//         }}
//       >
//         <Text>Click Change Language</Text>

//       </TouchableOpacity>
//     )
//   }
//   render() {
//     return (
//       <View style={styles.container}>
//       {this.heading()}
//       <View style={{flex: 2, width: '90%'}}>
//         <Text style={styles.text}>{I18n.t('greeting')}</Text>
//         <Text style={styles.text}>{I18n.t('title')}</Text>
//         <Text style={styles.text}>{I18n.t('Message')}</Text>
//         {this.button()}
//       </View>
//     </View>
//     )
//   }
// }

// export default App






import React, { Component } from 'react'
import {  Text, View } from 'react-native'

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Setting from './Setting';

import Stackhome from './Stackhome';
import Apple from './Apple';


const Tab=createBottomTabNavigator();
export class App extends Component {
  render() {
    return (
          <NavigationContainer>
            <Tab.Navigator>
            <Tab.Screen  name="home" component={Stackhome}/>
            {/* <Tab.Screen name="Apple" component={Apple}/> */}
             <Tab.Screen  name="setting" component={Setting}/>
              

            </Tab.Navigator>
          </NavigationContainer>
    )
  }
}

export default App
