/**
 * @format
 */

 import React from 'react'
import {AppRegistry} from 'react-native';
import { Provider } from 'react-redux';
import App from './App';
import {name as appName} from './app.json';
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { postArticleReducer } from './src/redux/reducer/postArticleReducer';


const store = createStore(postArticleReducer,applyMiddleware(thunk));


const Hello =()=>{
    return(
        <Provider store={store}>
            <App/>

        </Provider>
    )
}

AppRegistry.registerComponent(appName, () => Hello);
