import { createStackNavigator } from '@react-navigation/stack'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Apple from './Apple';
import Detail from './Detail';
import Homes from './Homes';


const stack= createStackNavigator();
export class Stackhome extends Component {
    render() {
        return (

            <stack.Navigator>
                <stack.Screen name="home" component={Homes}/>
                <stack.Screen name="detail" component={Detail}/>
                <stack.Screen name='Apple' component={Apple} />
            </stack.Navigator>
           
        )
    }
}

export default Stackhome
