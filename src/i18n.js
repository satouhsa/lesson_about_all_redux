import I18n, {getLanguages} from 'react-native-i18n';
import kh from './kh';
import en from './en';
import ko from './ko';

I18n.fallbacks=true;
I18n.translate={
    eg,
    kh,
    kr,

}

getLanguages()
.then((language) =>{
    console.log('get language' ,language);
})
.catch((error) =>{
    console.log('get language error ' ,error);
})


export default I18n;

