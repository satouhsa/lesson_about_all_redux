import Axios from "axios"


 const data=  {
    data:null,
}
export const postArticle=(object)=>{

    return(dispatch)=>{
        Axios.post('http://110.74.194.124:15011/v1/api/articles', object ,        
        {
            headers: {
            Authorization: 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=',
          },

        }).then(res =>{
            // data : res,
            
            console.log(res);
            dispatch({
                type:'ARTICLE_POST',
                data: res.data
            })
            
        })

        

    }

}

const success = (data) => {
    return {
        type: 'ARTCLE_GET',
        data: data,
    }
}

export const getArticle =()=>{

    console.log('getting');
    return(dispatch)=>{
        Axios.get('http://110.74.194.124:15011/v1/api/articles?page=1&limit=15',{
            headers: {
                Authorization: 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=',
              },
    

        }).then(result =>{
            console.log(result);
            dispatch(
            success(result.data)
        )
    }).catch( eror=>{
            console.log(eror);
           
        })

    }
}

export const deleteArticle =(id)=>{
    return(dispatch) =>{
        Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}` ,{
            headers:{
                Authorization:'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=',
            },
        
        }).then(resu =>{
            console.log("scess",resu);
            dispatch(
                {
                    type:'ARTICLE_DELETE',
                     data: resu.data,

                }
            )
        }).catch(error =>{
            console.log("error",error);
        })
    }

}


export const updateArticle =(object)=>{
    console.log("object",object);
   
    return(dispatch) =>{
        Axios.put(`http://110.74.194.124:15011/v1/api/articles/${object.id}`,object,{
            headers:{
                Authorization:'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=',
            },
        }).then(resul=>{
            dispatch({
                type:'ARTICLE_UPDATE',
                data:resul.data
            })
        })
    }
}