import React, { Component } from 'react'

import { Container, Header, Content, Card, CardItem, Body, Text, Form, View, } from 'native-base';
import axios from 'axios';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import Detail from './Detail';
import { getArticle,deleteArticle } from './src/redux/action/postArticle';
import { connect } from 'react-redux';
import {Alert, Button} from 'react-native';



const Stack = createStackNavigator();
 class Homes extends Component {

    constructor(){
        super();
        this.state={
           data:[],

        }
    }

    componentDidMount(){
        this.props.postData();
       

        // console.log(postData());

        // axios.get('https://jsonplaceholder.typicode.com/posts').then(res=>{
        //     console.log(res);
        //     this.setState({

        //         data:res.data,

        //     })


        // })
        

    }

    onclickdelete = (id) =>{

        console.log("this is",id);
        this.props.deleteData(id);
        



    }
    render() {
        const data = this.props.data?.data;
        
        console.log('render', this.props.data);

        const Buttonall =(props)=>{
            return(
                <View>
                    <Button title="delete"  onPress={(id)=>this.onclickdelete(props.item.id)}/>

                    <Button title="update" onPress={()=>this.props.navigation.navigate('Apple',{item: props.item})}/>
                </View>
            )

        }

        return (
            <Container>
                <Header />
                <Content>



                <FlatList
                    data={data}
                    keyExtractor={item => item.id + ''}
                    renderItem={({ item }) =>(
                        <Card>
                        {/* <TouchableOpacity >  */}
                            {/* onPress={()=> this.props.navigation.navigate('detail',{name:item}) } */}
                           
      
                        <CardItem>
                          <Body>
                              <Text style={{fontSize:20,fontWeight:'bold'}}>
                                      {item.title}
                              </Text>
                              <Text>
                                  {item.description}
                              </Text>
                          </Body>
                          <Buttonall item={item}/>
                          </CardItem>
      
                        {/* </TouchableOpacity>   */}
                          
                      </Card>
                        
                    )}
                    
                     />    
               
                </Content>
      </Container>
        )
    }
}




const mapStateToProps=(state)=>{
    return {
        data :state //map subcrip
    }
    



}
const statepro=(dispatch)=>{
    console.log('dispatching ...');
    return{
        postData: ()=> dispatch(getArticle()),
        deleteData: (id)=> dispatch(deleteArticle(id)),
    }
    


}

export default connect (mapStateToProps,statepro) (Homes)


